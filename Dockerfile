# Устанавливаем зависимости и билдим реактовый проект в первом контейнере
FROM node:current-alpine3.15 as build
WORKDIR /app
COPY package.json /app/package.json
RUN yarn install
COPY . /app
RUN yarn run build
# Перекладываем сбилженый проект в образ с ngnix и упаковываем в новый образ
FROM nginx:1.23.1
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
